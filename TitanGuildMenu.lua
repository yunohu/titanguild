----------------------------------------------------------------------
-- TitanGuildMenu.lua
-- code for generating the cascading right-click menus
----------------------------------------------------------------------

local L = LibStub("AceLocale-3.0"):GetLocale("Titan", true)

----------------------------------------------------------------------
-- TitanPanelRightClickMenu_PrepareGuildMenu()
----------------------------------------------------------------------
function TitanPanelRightClickMenu_PrepareGuildMenu()
	local NumGuild = 0;
	local guild_name = "";
	local guild_rank = "";
	local guild_rankIndex = "";
	local guild_level = "";
	local guild_class = "";
	local guild_zone = "";
	local guild_note = "";
	local guild_officernote = "";
	local guild_online = "";
	local guild_status = "";
	local guildIndex;



	-- Level 2 -----------------------------------------
	if ( TitanPanelRightClickMenu_GetDropdownLevel() == 2 ) then
		if (TitanPanelRightClickMenu_GetDropdMenuValue()) then
			-- chat
			if (TitanPanelRightClickMenu_GetDropdMenuValue() == TITAN_GUILD_MENU_CHAT_TEXT) then
				TitanPanelRightClickMenu_AddTitle(TitanPanelRightClickMenu_GetDropdMenuValue(), TitanPanelRightClickMenu_GetDropdownLevel());
				-- open guild chat
				info = {};
				info.text = TITAN_GUILD_GUILD_CHAT;
				info.func = function() TitanPanelGuildButton_OpenGuildChat(); end;
				TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				-- open officer chat only for officers
				local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
				-- assuming if the player can view the officer note, then they can do /o chat
				if ( C_GuildInfo.CanViewOfficerNote() ) then
					info = {};
					info.text = TITAN_GUILD_GUILD_OFFICER_CHAT;
					info.func = function() TitanPanelGuildButton_OpenGuildOfficerChat(); end;
					TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				end
			-- sort
			elseif (TitanPanelRightClickMenu_GetDropdMenuValue() == TITAN_GUILD_MENU_SORT_TEXT) then
				-- sort options
				TitanPanelRightClickMenu_AddTitle(TitanPanelRightClickMenu_GetDropdMenuValue(), TitanPanelRightClickMenu_GetDropdownLevel());
				local choiceIndex;
				for choiceIndex = 1, table.getn(sortChoicesValues) do
					info = {};
					info.text = sortChoicesLabels[choiceIndex];
					info.value = string.lower(sortChoicesValues[choiceIndex]);
					info.func = function() TitanPanelGuildButton_SetSortByValue(string.lower(sortChoicesValues[choiceIndex])); end;
					--info.checked = TitanGetVar(TITAN_GUILD_ID, "SortByValue");
					TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());					
				end
				TitanPanelRightClickMenu_AddCommand(TITAN_GUILD_MENU_HIDE, TITAN_GUILD_ID, TITAN_GUILD_BUTTON_CLOSEMENU, TitanPanelRightClickMenu_GetDropdownLevel());					
			elseif (TitanPanelRightClickMenu_GetDropdMenuValue() == TITAN_GUILD_MENU_TOOLTIP_TEXT) then
				-- tooltip options
				TitanPanelRightClickMenu_AddTitle(TitanPanelRightClickMenu_GetDropdMenuValue(), TitanPanelRightClickMenu_GetDropdownLevel());					
				local choiceIndex;
				for choiceIndex = 1, table.getn(sortChoicesValues) do
					info = {};
					info.text = sortChoicesLabels[choiceIndex];
					info.value = sortChoicesValues[choiceIndex];
					info.func = function() TitanPanelGuildButton_SetTooltipChoice(sortChoicesValues[choiceIndex]); end;
					info.checked = TitanGetVar(TITAN_GUILD_ID, "ShowTooltip"..sortChoicesValues[choiceIndex]);
					info.keepShownOnClick = 1;
					TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());					
				end
				info = {};
				info.text = TITAN_GUILD_MENU_DISPLAYSIZE:format( TitanGetVar(TITAN_GUILD_ID, "TooltipSize") );
				info.value = TITAN_GUILD_MENU_TOOLTIPSIZE;
				info.hasArrow = 1;
				TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				TitanPanelRightClickMenu_AddCommand(TITAN_GUILD_MENU_HIDE, TITAN_GUILD_ID, TITAN_GUILD_BUTTON_CLOSEMENU, TitanPanelRightClickMenu_GetDropdownLevel());
			elseif (TitanPanelRightClickMenu_GetDropdMenuValue() == TITAN_GUILD_MENU_FILTER_TEXT) then
				-- filter options
				TitanPanelRightClickMenu_AddTitle(TitanPanelRightClickMenu_GetDropdMenuValue(), TitanPanelRightClickMenu_GetDropdownLevel());
				-- lvl
				info = {};
				info.text = TITAN_GUILD_MENU_FILTER_MYLEVEL;
				info.func = function() TitanPanelGuildButton_ToggleFilterMyLevel(); end;
				info.checked = TitanGetVar(TITAN_GUILD_ID, "FilterMyLevel");
				TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				-- zone
				info = {};
				info.text = TITAN_GUILD_MENU_FILTER_MYZONE;
				info.func = function() TitanPanelGuildButton_ToggleFilterMyZone(); end;
				info.checked = TitanGetVar(TITAN_GUILD_ID, "FilterMyZone");
				TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				-- class
				info = {};
				info.text = TITAN_GUILD_MENU_FILTER_CLASS;
				info.hasArrow = 1;
				TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());												
				TitanPanelRightClickMenu_AddCommand(TITAN_GUILD_MENU_HIDE, TITAN_GUILD_ID, TITAN_GUILD_BUTTON_CLOSEMENU, TitanPanelRightClickMenu_GetDropdownLevel());					
			elseif (TitanPanelRightClickMenu_GetDropdMenuValue() == TITAN_GUILD_MENU_CONFIGURE_UPDATE_TIME) then
				-- update options
				TitanPanelRightClickMenu_AddTitle(TitanPanelRightClickMenu_GetDropdMenuValue(), TitanPanelRightClickMenu_GetDropdownLevel());
				local choiceIndex;
				for choiceIndex = 1, table.getn(updateTimeOptions) do
					info = {};
					info.text = updateTimeLabels[choiceIndex];
					info.value = updateTimeOptions[choiceIndex];
					info.func = function() TitanPanelGuildButton_SetRosterUpdateTime(updateTimeOptions[choiceIndex]); end;
					if (updateTimeOptions[choiceIndex] == TitanGetVar(TITAN_GUILD_ID, "RosterUpdateTime")) then
						info.checked = 1;
					end
					TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());					
				end
				-- toggle roster updates
				info = {};
				info.text = TITAN_GUILD_MENU_DISABLE_UPDATE_TEXT;
				info.func = function() TitanPanelGuildButton_ToggleRosterUpdates(); end;
				info.checked = TitanGetVar(TITAN_GUILD_ID, "DisableRosterUpdates");
				TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				-- toggle mouse-over updates
				info = {};
				info.text = TITAN_GUILD_MENU_DISABLE_MOUSEOVER_UPDATE;
				info.func = function() TitanPanelGuildButton_ToggleMouseOverUpdates(); end;
				info.checked = TitanGetVar(TITAN_GUILD_ID, "DisableMouseOverUpdates");
				TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());					
				-- hide							
				TitanPanelRightClickMenu_AddCommand(TITAN_GUILD_MENU_HIDE, TITAN_GUILD_ID, TITAN_GUILD_BUTTON_CLOSEMENU, TitanPanelRightClickMenu_GetDropdownLevel());
			-- player submenus
			else
				if (TitanGetVar(TITAN_GUILD_ID, "ShowAdvancedMenus")) then
					-- if hovering over a different rank, then refresh paging
					if (priorAdvMenuValue ~= TitanPanelRightClickMenu_GetDropdMenuValue()) then
						TitanPanelGuildButton_InitPaging();
						priorAdvMenuValue = TitanPanelRightClickMenu_GetDropdMenuValue();
					end
					-- generate lvl 2 player lists based on rank
					TitanPanelRightClickMenu_AddTitle(GuildControlGetRankName(TitanPanelRightClickMenu_GetDropdMenuValue()), TitanPanelRightClickMenu_GetDropdownLevel());
					TitanPanelGuildButton_ComputeAdvancedPages(table.getn(masterTable[TitanPanelRightClickMenu_GetDropdMenuValue()].members));
					TitanPanelGuildButton_BuildBackwardPageControl();
					for numMember = currIndex, maxIndex do
						if (masterTable[TitanPanelRightClickMenu_GetDropdMenuValue()].members[numMember]) then
							info = {};
							info.text = masterTable[TitanPanelRightClickMenu_GetDropdMenuValue()].members[numMember];
							info.hasArrow = 1;
							TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
						end				
					end
					TitanPanelGuildButton_BuildForwardPageControl();
				else
					-- build interaction menus for simple player list
					TitanPanelGuildButton_BuildInteractionMenu();
				end
			end
		end
		return
		-- End Level 2 -------------------------------------
	elseif (TitanPanelRightClickMenu_GetDropdownLevel() == 3) then
		-- Level 3 ------------------------------------------
		if (TitanPanelRightClickMenu_GetDropdMenuValue() == TITAN_GUILD_MENU_FILTER_CLASS) then
			TitanPanelGuildButton_BuildClassFilterMenu();
		else
			if (TitanPanelRightClickMenu_GetDropdMenuValue() == TITAN_GUILD_MENU_TOOLTIPSIZE) then
				local start = 5
				local size = TitanGetVar(TITAN_GUILD_ID, "TooltipSize");
				for i = 1, 11 do
					local index = start * i;
					info = {};
					info.text = index;
					info.value = index;
					if (size == index) then info.checked = 1; end
					info.func = TitanPanelGuildButton_SetTooltipSize;
					TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				end
			else
				TitanPanelGuildButton_BuildInteractionMenu();
			end
		end
		return									
		-- End Level 3 --------------------------------------
	end

	-- add a nice title for those that are not in a guild
	TitanPanelRightClickMenu_AddTitle(TitanPlugins[TITAN_GUILD_ID].menuText, TitanPanelRightClickMenu_GetDropdownLevel());

	if (IsInGuild()) then
		-- get guild members
		NumGuild = GetNumGuildMembers();
		-- build menus based on rank
		-- ADVANCED ---------------------------------------------			
		if (TitanGetVar(TITAN_GUILD_ID, "ShowAdvancedMenus")) then
			if (table.getn(masterTable) <= 0) then
				TitanPanelRightClickMenu_AddSpacer();
				TitanPanelRightClickMenu_AddTitle(TITAN_GUILD_MENU_PLEASE_WAIT_TEXT);
			else
				for rankMenuIndex = 1, table.getn(masterTable) do
					if (table.getn(masterTable[rankMenuIndex].members) > 0) then
						info = {};
						info.text = TitanPanelGuildButton_ColorRankNameText(rankMenuIndex-1, masterTable[rankMenuIndex].rank);
						info.hasArrow = 1;
						info.value = rankMenuIndex;
						TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
					end						
				end
			end
		else			
			-- build the lvl 1 simple menus
			-- SIMPLE ---------------------------------------------
			TitanPanelGuildButton_BuildBackwardPageControl();
			for guildIndex = currIndex, maxIndex do
				if (masterTableSimple[guildIndex]) then
					info = {};
					info.text = TitanPanelGuildButton_ColorRankNameText(masterTableSimple[guildIndex].rankIndex, masterTableSimple[guildIndex].name);
					info.hasArrow = 1;
					info.value = masterTableSimple[guildIndex].name;
					info.func = function() TitanPanelGuildButton_GuildWhisper(masterTableSimple[guildIndex].name) end;
					TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				end				
			end
			TitanPanelGuildButton_BuildForwardPageControl();
		end
	
		if (TitanGetVar(TITAN_GUILD_ID, "ShowMenuOptions")) then
			TitanPanelRightClickMenu_AddSpacer();

			-- toggle the menu options for more room
			TitanPanelRightClickMenu_AddCommand(TITAN_GUILD_MENU_HIDE_OPTIONS_TEXT, TITAN_GUILD_ID, TITAN_GUILD_BUTTON_SHOWOPTIONS);				
			TitanPanelRightClickMenu_AddSpacer();

			-- toggle the advanced menus
			info = {};
			info.text = TITAN_GUILD_MENU_SHOWADVANCED_TEXT;
			info.func = function() TitanPanelGuildButton_ToggleAdvancedMenus(); end;
			info.checked = TitanGetVar(TITAN_GUILD_ID, "ShowAdvancedMenus");
			TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());		
	
			-- open guild chat
			info = {};
			info.text = TITAN_GUILD_MENU_CHAT_TEXT;
			info.hasArrow = 1;
			TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				
			-- open sort submenu
			info = {};
			info.text = TITAN_GUILD_MENU_SORT_TEXT;
			info.hasArrow = 1;
			TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				
			-- open tooltip submenu
			info = {};
			info.text = TITAN_GUILD_MENU_TOOLTIP_TEXT;
			info.hasArrow = 1;
			TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
				
			-- open filter submenu
			info = {};
			info.text = TITAN_GUILD_MENU_FILTER_TEXT;
			info.hasArrow = 1;
			TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
			
			-- configure roster updates
			info = {};
			info.text = TITAN_GUILD_MENU_CONFIGURE_UPDATE_TIME;
			info.hasArrow = 1;
			TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());									
		else
			-- toggle the menu options for more room
			TitanPanelRightClickMenu_AddSpacer();
			TitanPanelRightClickMenu_AddCommand(TITAN_GUILD_MENU_SHOW_OPTIONS_TEXT, TITAN_GUILD_ID, TITAN_GUILD_BUTTON_SHOWOPTIONS);
		end
	end
	-- add default menu options
	TitanPanelRightClickMenu_AddSpacer();
        TitanPanelRightClickMenu_AddControlVars(TITAN_GUILD_ID);
	-- TitanPanelRightClickMenu_AddToggleIcon(TITAN_GUILD_ID);	
	-- TitanPanelRightClickMenu_AddToggleLabelText(TITAN_GUILD_ID);
	-- TitanPanelRightClickMenu_AddSpacer();
	-- TitanPanelRightClickMenu_AddCommand(L["TITAN_PANEL_MENU_HIDE"], TITAN_GUILD_ID, TITAN_PANEL_MENU_FUNC_HIDE);	
end

----------------------------------------------------------------------
--  TitanPanelGuildButton_BuildInteractionMenu()
----------------------------------------------------------------------
function TitanPanelGuildButton_BuildInteractionMenu()
	TitanPanelRightClickMenu_AddTitle(TitanPanelRightClickMenu_GetDropdMenuValue(), TitanPanelRightClickMenu_GetDropdownLevel());
	-- whisper
	info = {};
	info.text = TITAN_GUILD_MENU_ADVANCED_WHISPER_TEXT;
	info.value = TitanPanelRightClickMenu_GetDropdMenuValue();
	info.func = function() TitanPanelGuildButton_GuildWhisper(TitanPanelRightClickMenu_GetDropdMenuValue()) end;
	TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());			
	-- invite
	info = {};
	info.text = TITAN_GUILD_MENU_ADVANCED_INVITE_TEXT;
	info.value = TitanPanelRightClickMenu_GetDropdMenuValue();
	info.func = function() TitanPanelGuildButton_InviteToGroup(TitanPanelRightClickMenu_GetDropdMenuValue()) end;
	TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
	-- who
	info = {};
	info.text = TITAN_GUILD_MENU_ADVANCED_WHO_TEXT;
	info.value = TitanPanelRightClickMenu_GetDropdMenuValue();
	info.func = function() TitanPanelGuildButton_SendWhoRequest(TitanPanelRightClickMenu_GetDropdMenuValue()) end;
	TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());
	-- friend
	info = {};
	info.text = TITAN_GUILD_MENU_ADVANCED_FRIEND_TEXT;
	info.value = TitanPanelRightClickMenu_GetDropdMenuValue();
	info.func = function() TitanPanelGuildButton_AddFriend(TitanPanelRightClickMenu_GetDropdMenuValue()) end;
	TitanPanelRightClickMenu_AddButton(info, TitanPanelRightClickMenu_GetDropdownLevel());	
end
