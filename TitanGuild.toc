## Interface: 100002
## Title: Titan Panel [|cffeda55fGuild|r] |cff00aa0010.0.0.5|r
## Title-esES: Titan Panel [Hermandad]
## Notes: A simple guild list for the Titan Panel AddOn.
## Notes-esES: Una lista de la hermandad para el Titan Panel.
## Author: KanadiaN (aaron@kanadian.net), Maintainer
## Author: chicogrande (jluzier@gmail.com), port to 2.4 and further development by SeGeDel
## Author: kernighan (kernighan@gmail.com), 4.3 development and beyond
## SavedVariables:
## SavedVariablesPerCharacter: TitanGuildVar 
## OptionalDeps:
## Dependencies: Titan
## Version: 10.0.0.5
## X-Date: 2023-01-11
TitanGuild.xml
